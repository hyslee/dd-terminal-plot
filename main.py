#!/bin/python3

import sys, os
import numpy as np
import plotext as ple
import matplotlib.pyplot as plt
from simple_term_menu import TerminalMenu

def scanDirs(path):
    dirList = []
    with os.scandir(path) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_dir():
                dirList.append(entry.name)
    return dirList;

def callTermMenu(options):
    #options.append('Back to previous directory')
    options.insert(0,'Back to previous directory')
    terminal_menu = TerminalMenu(options)
    menu_entry_index = terminal_menu.show()
    # Press Q to exit the menu
    if menu_entry_index == None:
        print("Exiting...")
        exit()
    return options[menu_entry_index];

def main():
    # takes folder path as an argument
    if len(sys.argv)>1:
           DataPath = sys.argv[1]+'/'
    else:
        DataPath = './'

    # foldername 'evl' is used as a flag, once 'evl' folder is found, exit the menu
    IsThere_evl = False
    # scan the data directory 
    dirs = scanDirs(DataPath)
    # check if there is 'evl' folder in the list of directories that was scanned for the first time
    if 'evl' in dirs:
        IsThere_evl = True
        selectedDir = None
    # Continue to show the selection menu until the user selects the folder that includes 'evl'
    while not IsThere_evl:
        selectedDir = callTermMenu(dirs)
        if selectedDir == 'Back to previous directory':
            # append the previous directory path
            DataPath += './../'
        else:
            # append the selected folder name string to 'DataPath'
            DataPath += str(selectedDir)+'/'
        # scan the list of directories again
        dirs = scanDirs(DataPath)
        # if there is a folder called 'evl', set the flag to True
        if 'evl' in dirs:
            IsThere_evl = True

    # Call F data labels and save them as a list
    labels = np.loadtxt(f'{DataPath}/F/F_labels.txt', dtype='str', usecols=0)
    
    # Let the user select the plotting options
    terminal_menu = TerminalMenu(
        labels,
        multi_select=True,
        show_multi_select_hint=True,
    )
    # save the selected indices of the label list 
    menu_entry_indices = terminal_menu.show()

    # Load the plot data based on the user inputs 
    x_data = np.loadtxt(f'{DataPath}/F/F_0.txt', usecols=menu_entry_indices[0])
    y_data = np.loadtxt(f'{DataPath}/F/F_0.txt', usecols=menu_entry_indices[1])

    # Plot the data in terminal
    ple.theme('matrix')
    ple.plot(x_data, y_data)
    ple.title(selectedDir)
    ple.xlabel(labels[menu_entry_indices[0]])
    ple.ylabel(labels[menu_entry_indices[1]])
    ple.show()

    return 0;


if __name__ == '__main__':
    main()
