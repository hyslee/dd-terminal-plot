#!/bin/python3

import os
from simple_term_menu import TerminalMenu

#path = '/home/anon/Documents/Research/MoDELib/tutorials/DislocationDynamics'

def scanDirs(path):
    dirList = []
    with os.scandir(path) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_dir():
                dirList.append(entry.name)
    return dirList;

def callTermMenu(options):
    terminal_menu = TerminalMenu(options)
    menu_entry_index = terminal_menu.show()
    print(f"selected {options[menu_entry_index]}")
    return options[menu_entry_index];

def main():
    # Directory paths
    MoDELibpath='/home/anon/Documents/Research/MoDELib/'
    DataPath = MoDELibpath+'tutorials/DislocationDynamics/'

    IsThere_F_evl = False
    dirs = scanDirs(DataPath)
    while not IsThere_F_evl:
        dirs = scanDirs(DataPath)
        selectedDir = callTermMenu(dirs)
        DataPath += str(selectedDir)+'/'
        if 'evl' in scanDirs(DataPath):
            IsThere_F_evl = True

    print(DataPath)


    



if __name__=='__main__':
    main()
