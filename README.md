To run, first install the dependencies in requirements.txt

> pip install -r requirements.txt

If you want to run the code in current directory

> ./main.py

Or, you can pass the path as an argument

> ./main.py /path/to/data/

![demo](./images/demo.gif)

NOTE:
***never tested if it works on Windows
